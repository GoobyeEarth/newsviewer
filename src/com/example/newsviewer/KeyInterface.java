package com.example.newsviewer;

public interface KeyInterface {
	 String WORKSPACE = "/storage/emulated/0/Download/";
	 
	 //preff
	 String PREFERENCE_FILE = "preference_file";
	 
	 String PREF_CURRENT_DATE = "current_date";
	 String PREF_DATABASE= "database";
	 String PREF_TABLE_NAME = "table";
	 
	 String PREF_START_DATE = "start_date";
	 String PREF_END_DATE = "end_date";
	 
	 int TEXT_SIZE = 17;
}
