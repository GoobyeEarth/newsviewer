package com.example.newsviewer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.example.newsviewer.parts.DetailsDialogClass;
import com.example.newsviewer.parts.NewsDaoClass;
import com.example.newsviewer.parts.NewsDataClass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Layout;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import library.widget.ListDialogClass;
import library.widget.ListViewClass;
import library.widget.ListViewClass.ChildViewListener;
import library.widget.ListViewClass.ItemClickListener;
import library.widget.PreferenceClass;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class SelectedViewerActivity extends Activity implements KeyInterface{
	private Activity  activity= this;
	private Context context = this;
	
	private PreferenceClass pref;
	
	private NewsDaoClass  dao;
	private List<NewsDataClass> data;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		controller();

		mainview();

		listener();
	}
	private void controller(){
		pref = new PreferenceClass(context, PREFERENCE_FILE);
		
		String db = pref.loadString(PREF_DATABASE);
		String table = pref.loadString(PREF_TABLE_NAME);
		dao = new NewsDaoClass(context, db, table);
		data = dao.findChecked();
		
		
	}
	
	private void mainview(){
		ListViewClass listView = new ListViewClass(context);
		setContentView(listView);
		
		listView.setChildView(new ChildViewListener() {
				
				@Override
				public View setView(int position, View convertView) {
					LinearLayout linearLayout = new LinearLayout(context);
					linearLayout.setOrientation(LinearLayout.VERTICAL);
					
					
					TextView textView = new TextView(context);
					linearLayout.addView(textView);
					textView.setTextSize(TEXT_SIZE);
					textView.setTextColor(Color.BLACK);
					int padding = 10;

					textView.setPadding(padding, padding, padding, padding);
					textView.setTag(0);
					
					
			
					return linearLayout;
				}

				@Override
				public void setTextInputter(int position, View convertView, String[] strArray) {
					TextView textView = (TextView) convertView.findViewWithTag(0);
					textView.setText(strArray[0]);
				}
			});
		
		for (final NewsDataClass row : data) {
			listView.add(new String[]{row.title}, new ItemClickListener() {
				
				@Override
				public void onLongClick(int num, String[] textArray) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onClick(int num, String[] textArray, View view) {
					DetailsDialogClass.show(row, activity);
				}
			});
		}
		listView.setData();
		listView.setOnItemClickListener(null);
		
	}

	private void listener(){
		
	}

}