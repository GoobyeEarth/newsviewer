package com.example.newsviewer;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.Preference;
import android.text.Layout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import library.widget.ListDialogClass;
import library.widget.PreferenceClass;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class StartActivity extends Activity implements KeyInterface{
	private Activity  activity= this;
	private Context context = this;
	
	private PreferenceClass pref;
	
	private EditText dbEdit;
	private EditText tableEdit;
	private EditText dateEdit;
	private Button startButton;
	private Button seeCheckButton;
	
	private Button progressButton;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		controller();

		mainview();
		
		textSet();
		
		listener();
		
	}
	
	private void controller(){
		pref = new PreferenceClass(context, PREFERENCE_FILE);
		
	}
	
	private void mainview(){
		LinearLayout mainLinear = new LinearLayout(context);
		mainLinear.setOrientation(LinearLayout.VERTICAL);
		setContentView(mainLinear);
		
		dbEdit = new EditText(context);
		LinearLayout.LayoutParams dbLP = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		mainLinear.addView(dbEdit, dbLP);
		
		tableEdit = new EditText(context);
		LinearLayout.LayoutParams tableLP = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		mainLinear.addView(tableEdit, tableLP);
		
		dateEdit = new EditText(context);
		LinearLayout.LayoutParams dateLP = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		mainLinear.addView(dateEdit, dateLP);
		
		startButton = new Button(context);
		LinearLayout.LayoutParams startLP = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1);
		mainLinear.addView(startButton, startLP);
		
		seeCheckButton = new Button(context);
		LinearLayout.LayoutParams seeCheckLP = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1);
		mainLinear.addView(seeCheckButton, seeCheckLP);
		
		progressButton = new Button(context);
		LinearLayout.LayoutParams pLP = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1);
		mainLinear.addView(progressButton, pLP);
	
	}
	
	private void textSet() {
		
		dbEdit.setText(pref.loadString(PREF_DATABASE));
		dbEdit.setHint("database");
		tableEdit.setText(pref.loadString(PREF_TABLE_NAME));
		tableEdit.setHint("table");
		dateEdit.setText(pref.loadString(PREF_CURRENT_DATE));
		dateEdit.setHint("date");
		startButton.setText("START");
		seeCheckButton.setText("see checked news");
		progressButton.setText("see calendar");
		
		
	}
	
	private void listener(){
		startButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String db = dbEdit.getText().toString();
				String table = tableEdit.getText().toString();
				String date = dateEdit.getText().toString();
				pref.saveString(PREF_DATABASE, db);
				pref.saveString(PREF_TABLE_NAME, table);
				pref.saveString(PREF_CURRENT_DATE, date);
				
				Intent intent = new Intent(context, ViewerActivity.class);
				startActivity(intent);
				finish();
			}
		});
		
		seeCheckButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String db = dbEdit.getText().toString();
				String table = tableEdit.getText().toString();
				String date = dateEdit.getText().toString();
				pref.saveString(PREF_DATABASE, db);
				pref.saveString(PREF_TABLE_NAME, table);
				pref.saveString(PREF_CURRENT_DATE, date);
				
				Intent intent = new Intent(context, SelectedViewerActivity.class);
				startActivity(intent);
				
			}
		});
		
		progressButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, ProgressActivity.class);
			
				
				startActivity(intent);
				
			}
		});
	}

}
