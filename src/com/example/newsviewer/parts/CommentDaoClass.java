package com.example.newsviewer.parts;
import java.util.ArrayList;
import java.util.List;

import com.example.newsviewer.KeyInterface;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import library.widget.DaoAbstractClass;


public class CommentDaoClass extends DaoAbstractClass implements KeyInterface{
	private static String DATABASE_NAME = WORKSPACE + PREF_DATABASE;
	private static final String TABLE_NAME = "comment_table";
	
	private static final String COLUMN_DATE = "date";
	private static final String COLUMN_COMMENT = "comment";
	

	private static final int NUM_DATE = 0;
	private static final int NUM_COMMENT = 1;
	
	
	private static final String[] COLUMN = new String[]{
		COLUMN_DATE,
		COLUMN_COMMENT
	};

	private static final int[] TYPES = new int[]{
		TYPE_STRING,
		TYPE_STRING
	};
	
	public CommentDaoClass(Context context, String databaseName){
		super(context, WORKSPACE + databaseName, TABLE_NAME, COLUMN, TYPES);
		
		DATABASE_NAME = WORKSPACE + databaseName;
	}
	
	/**
	 * 全データの取得 ----------------①
	 *
	 * @return
	 */
	public List<CommentDataClass> findAll(){
		List<CommentDataClass> entityList = new ArrayList<CommentDataClass>();

		Cursor cursor = db.query(TABLE_NAME, column, null, null, null, null, null);

		while (cursor.moveToNext()) {
			entityList.add(getDataClass(cursor) );
		}

		return entityList;
	}

	
	public CommentDataClass findByDate(String date){
		String selection = COLUMN_DATE + "=" + "'" + date + "'";
		Cursor cursor = db.query(TABLE_NAME, column, selection, null, null,
				null, null);

		/*
		 * to start id with 1.
		 */
		if(cursor.moveToNext() ){
			return getDataClass(cursor);
		}
		else{
			CommentDataClass entity = new CommentDataClass();
			entity.comment = "";
			entity.date = date;
			return entity;
		}

	}

	private CommentDataClass getDataClass(Cursor cursor){
		CommentDataClass entity = new CommentDataClass();
		entity.date = cursor.getString(NUM_DATE);
		entity.comment = cursor.getString(NUM_COMMENT);


		return entity;
	}

	/**
	 * データの登録 ----------------③
	 *
	 * @param data
	 * @return
	 */
	public long insert(CommentDataClass entity){

		return db.insert(TABLE_NAME, null, getValues(entity) );
		
	}

	/**
	 * データの更新 ----------------④
	 *
	 * @param rowid
	 * @param date
	 * @return
	 */
	public int update(CommentDataClass entity) {
		String whereClause = COLUMN_DATE + "=" + "'"+  entity.date + "'";
		return db.update(TABLE_NAME, getValues(entity), whereClause, null);
	}

	private ContentValues getValues(CommentDataClass entity){
		ContentValues values = new ContentValues();

		
		values.put(COLUMN_DATE, entity.date);
		values.put(COLUMN_COMMENT, entity.comment);
		
		return values;

	}




	/**
	 * データ数
	 * @return
	 */
   public long getRecordCount() {
       String sql = "select count(*) from " + TABLE_NAME;
       Cursor c = db.rawQuery(sql, null);
       c.moveToLast();
       long count = c.getLong(0);
       c.close(); 
       return count;
   }
   
   public boolean isComment(String date){
	   String sql = "select count(*) from " + TABLE_NAME + " where date = '" + date  + "'";
       Cursor c = db.rawQuery(sql, null);
       c.moveToLast();
       long count = c.getLong(0);
       c.close(); 
       return count == 1;
   }
   
	
	public boolean getEntityExists(String column, String sig, String right){
		  String sql = "select count(*) from " + TABLE_NAME + " WHERE " + column + sig + "'" + right + "'"; 
	      Cursor c = db.rawQuery(sql, null);
	      c.moveToLast();
	      long count = c.getLong(0);
	      c.close();
	      if(count >= 1){
	    	  return true;
	      }
	      else{
	    	  return false;
	      }
	  }
	
	public boolean saveComment(CommentDataClass entity){
		boolean commentExists =  isComment(entity.date);
		
		if(commentExists){
			update(entity);
		}
		else{
			insert(entity);
		}
		return commentExists;
	}
	
	
}
