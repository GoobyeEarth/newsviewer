package com.example.newsviewer.parts;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import library.widget.ListDialogClass;

public class DetailsDialogClass{
	public static void show(final NewsDataClass data, final Activity activity){
		ListDialogClass dialog = new ListDialogClass(activity, data.title);
		dialog.addItem(data.date + " " + data.doWeek + " " + data.time
				+ "\tcheck: " + data.check, null);
		dialog.addItem(data.details, null);
		
		dialog.addItem(data.url, new ListDialogClass.ClickListener() {
			
			@Override
			public void setProcess(int num, String str) {
				Uri uri = Uri.parse(data.url);
				Intent i = new Intent(Intent.ACTION_VIEW,uri);
				activity.startActivity(i);
			}
		});
		
		dialog.show(null);
	}
}
