package com.example.newsviewer.parts;
import java.util.ArrayList;
import java.util.List;

import com.example.newsviewer.KeyInterface;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import library.widget.DaoAbstractClass;


public class ProgressDaoClass extends DaoAbstractClass implements KeyInterface{
	private static String DATABASE_NAME = WORKSPACE + PREF_DATABASE;
	private static final String TABLE_NAME = "progress_table";
	
	private static final String COLUMN_SEEING = "seeing";
	private static final String COLUMN_ARCHIVE = "archive";
	

	private static final int NUM_SEEING = 0;
	private static final int NUM_ARCHIVE = 1;
	
	
	private static final String[] COLUMN = new String[]{
		COLUMN_SEEING,
		COLUMN_ARCHIVE
	};

	private static final int[] TYPES = new int[]{
		TYPE_STRING,
		TYPE_STRING
	};
	
	public ProgressDaoClass(Context context, String databaseName){
		super(context, WORKSPACE + databaseName, TABLE_NAME, COLUMN, TYPES);
		
		DATABASE_NAME = WORKSPACE + databaseName;
	}
	
	/**
	 * 全データの取得 ----------------①
	 *
	 * @return
	 */
	public List<ProgressDataClass> findAll(){
		List<ProgressDataClass> entityList = new ArrayList<ProgressDataClass>();

		Cursor cursor = db.query(TABLE_NAME, column, null, null, null, null, null);

		while (cursor.moveToNext()) {
			entityList.add(getDataClass(cursor) );
		}

		return entityList;
	}
	
	public List<String> findDistinctArchiveDate(){
		List<String> entityList = new ArrayList<String>();
		String sql = "select distinct " + COLUMN_ARCHIVE + " from " + TABLE_NAME; 
		Cursor c = db.rawQuery(sql, null);

		while (c.moveToNext()) {
			entityList.add(c.getString(0) );
		}

		return entityList;
	}
	
	
	
	
	
	public ProgressDataClass findByDate(String date){
		String selection = COLUMN_ARCHIVE + "=" + "'" + date + "'";
		Cursor cursor = db.query(TABLE_NAME, column, selection, null, null,
				null, null);

		/*
		 * to start id with 1.
		 */
		if(cursor.moveToNext() ){
			return getDataClass(cursor);
		}
		else{
			ProgressDataClass entity = new ProgressDataClass();
			return entity;
		}

	}

	private ProgressDataClass getDataClass(Cursor cursor){
		ProgressDataClass entity = new ProgressDataClass();
		entity.seeingDate = cursor.getString(NUM_SEEING);
		entity.archiveDate = cursor.getString(NUM_ARCHIVE);


		return entity;
	}

	/**
	 * データの登録 ----------------③
	 *
	 * @param data
	 * @return
	 */
	public long insert(ProgressDataClass entity){

		return db.insert(TABLE_NAME, null, getValues(entity) );
		
	}

	/**
	 * データの更新 ----------------④
	 *
	 * @param rowid
	 * @param date
	 * @return
	 */
	public int update(ProgressDataClass entity) {
		String whereClause = COLUMN_ARCHIVE + "=" + "'"+  entity.archiveDate + "'";
		return db.update(TABLE_NAME, getValues(entity), whereClause, null);
	}

	private ContentValues getValues(ProgressDataClass entity){
		ContentValues values = new ContentValues();

		
		values.put(COLUMN_SEEING, entity.seeingDate);
		values.put(COLUMN_ARCHIVE, entity.archiveDate);
		
		return values;

	}




	/**
	 * データ数
	 * @return
	 */
   public long getRecordCount() {
       String sql = "select count(*) from " + TABLE_NAME;
       Cursor c = db.rawQuery(sql, null);
       c.moveToLast();
       long count = c.getLong(0);
       c.close(); 
       return count;
   }
   
   public boolean exists(String date){
	   String sql = "select count(*) from " + TABLE_NAME + " where archive = '" + date  + "'";
       Cursor c = db.rawQuery(sql, null);
       c.moveToLast();
       long count = c.getLong(0);
       c.close(); 
       return count == 1;
   }
   
	
	public boolean getEntityExists(String column, String sig, String right){
		  String sql = "select count(*) from " + TABLE_NAME + " WHERE " + column + sig + "'" + right + "'"; 
	      Cursor c = db.rawQuery(sql, null);
	      c.moveToLast();
	      long count = c.getLong(0);
	      c.close();
	      if(count >= 1){
	    	  return true;
	      }
	      else{
	    	  return false;
	      }
	  }
	
	
	
	public boolean saveProgress(ProgressDataClass entity){
		boolean commentExists =  exists(entity.archiveDate);
		
		if(commentExists){
			update(entity);
		}
		else{
			insert(entity);
		}
		return commentExists;
	}
	
	
}
